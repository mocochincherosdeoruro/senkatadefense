using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicaRaza : MonoBehaviour
{
    public float tiempovida;
    public GameObject Raza;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        tiempovida -= Time.deltaTime;
        if(tiempovida<=0)
        {
            Instantiate(Raza, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}
