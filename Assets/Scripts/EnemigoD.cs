using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Code
{
    public class EnemigoD : MonoBehaviour
    {
        public GameObject Muere;
        public Collider eCadena;
        public Animator Anim;
        public GameObject Character;
        public GameObject Explosion;
        public GameObject HuidaE;
        public NavMeshAgent Llunku;
        public int vida;
        public GameObject Inicio;
        public GameObject[] Amenazas = new GameObject[3];
        public GameObject[] Exclamasiones = new GameObject[4];
        
        public GameObject[] Sonidos = new GameObject[6];
        public GameObject[] PuntoA = new GameObject[4];
        public GameObject[] PuntoB = new GameObject[5];
        public GameObject[] PuntoC = new GameObject[4];
        public GameObject[] PuntoD = new GameObject[2];
        int rEleccion;
        bool sondioOff = false;
        public GameObject Actual;
        public int index;
        float tiempoEs;
        int RandomPowerUp;
        bool PuntoC1;
        bool PuntoC2;
        bool PuntoC3;
        bool PuntoC4;

        public GameObject Polecia;
        // Start is called before the first frame update
        void Start()
        {

            tiempoEs = 2.0f;
            eCadena.enabled = false;
            PuntoC1 = true;
            PuntoC2 = false;
            PuntoC3 = false;
            PuntoC4 = false;
            index = Random.Range(0, 4);
            Actual = PuntoA[index];
            //Instantiate(Inicio, transform.position, transform.rotation);
        }

        // Update is called once per frame
        void Update()
        {
            transform.LookAt(Polecia.transform);
            if (PuntoC1 == true)
            {

                Llunku.destination = Actual.transform.position;

                if (Vector3.Distance(transform.position, Llunku.destination) <= 1f)
                {
                    PuntoC1 = false;
                    PuntoC2 = true;
                    index = Random.Range(0, 5);
                    Huir();
                    Actual = PuntoB[index];
                }

            }
            else
            {
                if (PuntoC2 == true)
                {
                    Llunku.destination = Actual.transform.position;
                    if (Vector3.Distance(transform.position, Llunku.destination) <= 1f)
                    {
                        rEleccion = Random.Range(0, 5);
                        if (rEleccion == 4)
                        {
                            Instantiate(Amenazas[Random.Range(0, 3)], transform.position, transform.rotation);
                        }
                        PuntoC2 = false;
                        PuntoC3 = true;
                        index = Random.Range(0, 4);
                        Huir();
                        Actual = PuntoC[index];
                    }
                }
                else
                {
                    if (PuntoC3 == true)
                    {
                        Llunku.destination = Actual.transform.position;
                        if (Vector3.Distance(transform.position, Llunku.destination) <= 1f)
                        {
                            PuntoC3 = false;
                            PuntoC4 = true;
                            index = Random.Range(0, 2);
                            Actual = PuntoD[index];
                            Huir();
                        }
                    }
                    else
                    {
                        if (PuntoC4 == true)
                        {
                            Llunku.destination = Actual.transform.position;
                            if (Vector3.Distance(transform.position, Llunku.destination) <= 1f)
                            {
                                FindObjectOfType<Hidden>().AltoBoom();
                              //  FindObjectOfType<Contador>().FinJuego();
                                Time.timeScale = 0;
                            }
                        }

                    }
                }
            }
        }
        public void Llamada(int a)
        {
            if (a == 180)
            {
                Character.SetActive(false);
                Instantiate(Explosion, new Vector3(transform.position.x, 1.0f, transform.position.z), transform.rotation);
                eCadena.enabled = true;
                Destroy(Character, 0.0f);
            }
            
            if (a == 40)
            {

                RandomPowerUp = Random.Range(1, 100);
                Polecia.GetComponent<RotacionAR>().RShoot(RandomPowerUp);
            }
            vida = vida - a;
            if (vida <= 0 && a!=0)
            {
                if (sondioOff == false)
                {
                    Llunku.speed = 0.0f;
                    //Anim.SetBool("seMuere", true);
                    Character.SetActive(false);
                    Instantiate(Muere, new Vector3(transform.position.x, 0.0f, transform.position.z), transform.rotation);
                    Instantiate(Exclamasiones[Random.Range(0, 4)], transform.position, transform.rotation);
                    Destroy(Character, 1.0f);
                    sondioOff = true;
                }

            }
            else
            {
                if (sondioOff == false)
                {
                    Llunku.speed = 1.5f;
                    Instantiate(Sonidos[Random.Range(0, 6)], transform.position, transform.rotation);
                    sondioOff = true;
                }
                else
                {
                    tiempoEs = tiempoEs - Time.deltaTime;
                    if (tiempoEs <= 0)
                    {
                        sondioOff = false;
                        tiempoEs = 2.5f;
                    }
                }

            }
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == ("Llunku"))
            {
                Character.SetActive(false);
                Instantiate(Explosion, new Vector3(transform.position.x, 1.0f, transform.position.z), transform.rotation);

                Destroy(Character, 0.0f);
            }
        }
        public void Huir()
        {
            if(Random.Range(1,10)==5)
            {
                Character.SetActive(false);
                Instantiate(HuidaE, new Vector3(transform.position.x, 1.0f, transform.position.z), transform.rotation);
            }
        }
    }

}
