using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hidden : MonoBehaviour
{
    public GameObject Canvas1;
    public GameObject Canvas2;
    public GameObject CanvasPause;
    public GameObject CanvasFinal;
    // Start is called before the first frame update
    void Start()
    {
        Canvas1.SetActive(true);
        Canvas2.SetActive(true);
        CanvasPause.SetActive(false);
        CanvasFinal.SetActive(false);
    }

    // Update is called once per frame
    public void AltoBoom()
    {
        Cursor.visible = true;
        Canvas1.SetActive(false);
        Canvas2.SetActive(false);
        CanvasPause.SetActive(false);
        CanvasFinal.SetActive(true);
    }
}
