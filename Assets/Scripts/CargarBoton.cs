using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CargarBoton : MonoBehaviour
{
    [SerializeField] private Button ButtonMelgarejo;
    [SerializeField] private Button ButtonRacista;
    void Start()
    {
        AddListeners();
    }
    public void CargarNivel(int NumeroDeEscena)
    {
        Debug.Log("Escena Cargada");
        SceneManager.LoadScene(NumeroDeEscena);
    }
    private void AddListeners()
    {
        ButtonMelgarejo.onClick.AddListener(CargarMelgarejo);
        ButtonRacista.onClick.AddListener(CargarMasistas);
    }
    private void CargarMelgarejo()
    {
        SceneManager.LoadScene("4 a melgarejoda");
    }

    private void CargarMasistas()
    {
        SceneManager.LoadScene("4 a senkata");
    }
}
