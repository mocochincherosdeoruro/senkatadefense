using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Code
{
    public class EnemigoP : MonoBehaviour
    {
        public SpriteRenderer giro;
        public float Vel;
        private float espera;
        private float espera2;
        public float inesp;
        private int i = 0;
        public Transform[] puntos;
        private Vector2 PosAC;
        bool dang;
        bool rot;
        public int Puntos;
        public GameObject PuntosOTP;
        public GameObject Clinck;
        public int Flip;

        // Start is called before the first frame update
        void Start()
        {
            espera = 3f;
            espera2 = 3f;
            dang = false;
            rot = true;
        }

        // Update is called once per frame
        void Update()
        {
            if (dang == false)
            {
                transform.position = Vector3.MoveTowards(transform.position,
                                                     new Vector3(puntos[i].transform.position.x, transform.position.y, transform.position.z),
                                                     Vel * Time.deltaTime);
                if (Vector2.Distance(transform.position, puntos[i].transform.position) < 0.1f)
                {

                    if (espera <= 0)
                    {

                        if (puntos[i] != puntos[puntos.Length - 1])
                        {
                            giro.flipX = true;
                            i++;
                        }
                        else
                        {
                            giro.flipX = false;
                            i = 0;
                        }
                        espera = inesp;
                    }
                    else
                    {
                        espera -= Time.deltaTime;
                    }
                }
            }
            else
            {
                if (rot == true)
                {
                    transform.Rotate(-90.0f * Flip, 0.0f, 0f, Space.Self) ;
                    Instantiate(Clinck, transform.position, transform.rotation);
                    rot = false;
                }
                else
                {
                    if (espera2 >= 0)
                    {
                        espera2 = espera2 - Time.deltaTime;
                    }
                    else
                    {
                        espera2 = 3f;
                        transform.Rotate(90.0f*Flip, 0.0f, 0.0f, Space.Self);
                        dang = false;
                        rot = true;
                    }
                }
            }

        }

        public void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.tag == ("Bala"))
            {
                if (dang == false)
                {
                    Velocidad(2f);
                    PuntosOTP.GetComponent<Contador>().ContadorPuntos(Puntos);
                    dang = true;
                }
            }

        }
        public void Velocidad(float a)
        {
            Vel = Vel + a;
        }
    }
}

