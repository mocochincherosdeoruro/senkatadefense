using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorCambio : MonoBehaviour
{
    public Texture2D CursorAp;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.SetCursor(CursorAp, Vector2.zero, CursorMode.ForceSoftware);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
