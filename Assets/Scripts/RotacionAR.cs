using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class RotacionAR : MonoBehaviour
{
    [SerializeField]
    public Transform Arma;
    public Rigidbody Bala;

    public Rigidbody Bala2;
    public Transform PuntoS;
    public GameObject Mouser;
    public GameObject Bombones;
    bool active;
    int cant_b;
    public GameObject[] Sound;
    float timeR;
    float timeS;
    float timeR2;
    float timeS2;
    int var_b;
    int cant_s;
    int var_s;
    float timeRec;
    float timeRec2;
    int probF;
    int probS;
    bool Reload = false;

    public Text TextBall;
    public Text Probabilidades;


    // Start is called before the first frame update
    void Start()
    {
        Probabilidades.text = "Probabilidad actual: " + probF + " F, " + probS + " S";
        TextBall.text = "x: " + cant_b;
        timeR = 3.5f;
        timeS = 1f;
        timeR2 = 5f;
        timeS2 = 2.3f;
        cant_b = 5;
        var_b = 5;
        Mouser.SetActive(true);
        active = true;
        Bombones.SetActive(false);
        cant_s = 20;
        var_s = 20;
        timeRec = 3.5f;
        timeRec2 = 5.0f;
        probF = 20;
        probS = 10;
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Ray Rasho = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit Info;
        
        if (Input.GetButton("Alpha1"))
        {
            Mouser.SetActive(true);
            active = true;
            Bombones.SetActive(false);
        }
        if(Input.GetButton("Alpha2"))
        {
            Mouser.SetActive(false);
            active = false;
            Bombones.SetActive(true);
        }
        
        if (Physics.Raycast(Rasho, out Info))
        {

            if(Info.collider!=null)
            {
                Vector3 direccion = Info.point - Arma.position;
                Arma.rotation = Quaternion.LookRotation(direccion);
                if (active == true)
                {
                    TextBall.text = "x: " + cant_b;
                    if (cant_b > 0)
                    {
                        if (Input.GetMouseButtonDown(0))
                        {
                            Rigidbody _Bala;
                            _Bala = Instantiate(Bala, PuntoS.position, Arma.rotation) as Rigidbody;
                            _Bala.AddForce(PuntoS.up * 5000f);
                            Destroy(_Bala, 0.9f);
                            Instantiate(Sound[0], PuntoS.position, Arma.rotation);
                            cant_b = cant_b - 1;
                            TextBall.text = "x: " + cant_b;
                            timeS = timeS- Time.deltaTime;
                            if (timeS < 0)
                            {
                                timeS = 1f;
                            }
                        }
                    }
                    else
                    {
                        if(Reload==false)
                        {

                            Instantiate(Sound[1], PuntoS.position, Arma.rotation);
                        }
                        Reload = true;
                        timeR = timeR - Time.deltaTime;
                        if (timeR < 0)
                        {
                            Reload = false;
                            cant_b = var_b;
                            timeR = timeRec;
                            TextBall.text = "x: " + cant_b;
                        }

                    }
                    

                }
                else
                {
                    TextBall.text = "x: " + cant_s;
                    if (cant_s > 0)
                    {
                        if (Input.GetMouseButton(0))
                        {
                            Rigidbody _Bala;
                            _Bala = Instantiate(Bala2, PuntoS.position, Arma.rotation) as Rigidbody;
                            _Bala.AddForce(PuntoS.up * 5000f);
                            Destroy(_Bala, 0.9f);
                            //Instantiate(Sound, PuntoS.position, Arma.rotation);
                            cant_s = cant_s - 1;
                            TextBall.text = "x: " + cant_s;
                            timeS2 = timeS2 - Time.deltaTime;
                            if (timeS2 < 0)
                            {
                                timeS2 = 2.3f;
                            }
                        }
                    }
                    else
                    {
                        timeR2 = timeR2 - Time.deltaTime;
                        if (timeR2 < 0)
                        {
                            cant_s = var_s;
                            timeR2 = timeRec2;
                            TextBall.text = "x: " + cant_s;
                        }

                    }
                }

            }
        }   
    }
    public void RShoot(int a)
    {
        if(active==true)
        {
            if(a>=1 && a<=probF)
            {
                
                cant_b = cant_b +1 ;
            }
            
        }
        else
        {
            if (a >= 1 && a <= probS)
            {
                cant_s = cant_s + 5;
            }
        }
        
    }
    public void InstantR(int a)
    {
        if (active == true)
        {
            probF = probF + 5;
            Probabilidades.text = "Probabilidad actual: " + probF +" F, " + probS + " S";

        }
        else
        {
            probS = probS + 5;
        }
    }
}
