using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala_Detector : MonoBehaviour
{
    public GameObject Bala;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void OnCollisionEnter(Collision other)
    {
        Destroy(Bala, 0.0f);
    }

}
