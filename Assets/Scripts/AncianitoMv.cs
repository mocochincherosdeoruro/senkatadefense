using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Code
{
    public class AncianitoMv : MonoBehaviour
    {

        public GameObject Character;
        public GameObject Morido;
        public UnityEngine.AI.NavMeshAgent Llunku;
        public int vida;
        public GameObject[] PuntoA = new GameObject[4];
        public GameObject[] PuntoB = new GameObject[5];
        public GameObject[] PuntoC = new GameObject[4];
        public GameObject[] PuntoD = new GameObject[2];

        public GameObject Actual;
        public int index;

        bool PuntoC1;
        bool PuntoC2;
        bool PuntoC3;
        bool PuntoC4;



        public GameObject PuntosOTP;

        public GameObject Polecia;
        // Start is called before the first frame update
        void Start()
        {

            PuntoC1 = true;
            PuntoC2 = false;
            PuntoC3 = false;
            PuntoC4 = false;
            index = Random.Range(0, 4);
            Actual = PuntoA[index];
        }

        // Update is called once per frame
        void Update()
        {
            transform.LookAt(Polecia.transform);
            if (PuntoC1 == true)
            {

                Llunku.destination = Actual.transform.position;

                if (Vector3.Distance(transform.position, Llunku.destination) <= 1f)
                {
                    PuntoC1 = false;
                    PuntoC2 = true;
                    index = Random.Range(0, 5);
                    Actual = PuntoB[index];
                }

            }
            else
            {
                if (PuntoC2 == true)
                {
                    Llunku.destination = Actual.transform.position;
                    if (Vector3.Distance(transform.position, Llunku.destination) <= 1f)
                    {
                        PuntoC2 = false;
                        PuntoC3 = true;
                        index = Random.Range(0, 4);
                        Actual = PuntoC[index];
                    }
                }
                else
                {
                    if (PuntoC3 == true)
                    {
                        Llunku.destination = Actual.transform.position;
                        if (Vector3.Distance(transform.position, Llunku.destination) <= 1f)
                        {
                            PuntoC3 = false;
                            PuntoC4 = true;
                            index = Random.Range(0, 2);
                            Actual = PuntoD[index];
                        }
                    }
                    else
                    {
                        if (PuntoC4 == true)
                        {
                            Llunku.destination = Actual.transform.position;
                            if (Vector3.Distance(transform.position, Llunku.destination) <= 1f)
                            {

                                PuntosOTP.GetComponent<Contador>().ContadorPuntos(50);
                                Polecia.GetComponent<RotacionAR>().InstantR(1);
                                Destroy(Character, 0.0f);

                            }
                        }

                    }
                }
            }
        }

        public void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.tag == ("Bala"))

            {
                Llunku.speed = 0.0f;
                Character.SetActive(false);
                Instantiate(Morido, new Vector3(transform.position.x, 1.0f, transform.position.z), transform.rotation);

                Destroy(Character, 0.1f);
                PuntosOTP.GetComponent<Contador>().ContadorPuntos(-70);
            }
        }
    }
}

