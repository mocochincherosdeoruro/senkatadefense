using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneMulti : MonoBehaviour
{
    
    public void Cambiar(int Scena)
    {
        SceneManager.LoadScene(Scena);
    }
    public void CerrarESC()
    {
        Application.Quit();
    }
}
