using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Code
{
    public class CollisionInstructor : MonoBehaviour
    {
        public int Danio;
        public int Danio2;
        public int Puntos;
        public int Puntos2;
        public GameObject EnemigoL;
        public GameObject PuntosOTP;

        public GameObject TextoCaida;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        public void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.tag == ("Bala"))
            {
                EnemigoL.GetComponent<EnemigoD>().Llamada(Danio);
                TextoCaida.GetComponent<TextMesh>().text = "" + Danio;
                Instantiate(TextoCaida, transform.position, transform.rotation);
                PuntosOTP.GetComponent<Contador>().ContadorPuntos(Puntos);
            }
            if (other.gameObject.tag == ("Bala2"))
            {
                EnemigoL.GetComponent<EnemigoD>().Llamada(Danio2);
                TextoCaida.GetComponent<TextMesh>().text = "" + Danio2;
                Instantiate(TextoCaida, transform.position, transform.rotation);
                PuntosOTP.GetComponent<Contador>().ContadorPuntos(Puntos2);
            }
        }

    }
}

