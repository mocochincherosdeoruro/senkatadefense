using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnP : MonoBehaviour
{
    public GameObject Enemigo;
    public GameObject Chokito;
    public int nRespawner;
    int TipoEn;
    public int CantEn;
    public float timerR;
    int probLlunku;
    // Start is called before the first frame update
    void Start()
    {
        TipoEn = Random.Range(0, 3);
        CantEn = Random.Range(1, 4);
        timerR = Random.Range(5f, 15f);
    }

    // Update is called once per frame
    void Update()
    {
        timerR -= Time.deltaTime;
        if (timerR <= 0)
        {
            while (CantEn >= 0)
            {
                GameObject cEnemy;
                cEnemy = Instantiate(Chokito, transform.position, transform.rotation) as GameObject;
                CantEn--;
                probLlunku = Random.Range(0,5);
                if(probLlunku==3)
                {
                    cEnemy = Instantiate(Enemigo, transform.position, transform.rotation) as GameObject;
  
                }
            }
            CantEn = Random.Range(1, 4);
            timerR = Random.Range(5f, 15f);
        }
        
    }
}
