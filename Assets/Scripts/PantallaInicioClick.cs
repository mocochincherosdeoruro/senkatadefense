using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PantallaInicioClick : MonoBehaviour
{
    public GameObject MenuInicio;
    
    public GameObject PantallaInicio;
    void Start()
    {
        Time.timeScale = 1;
        MenuInicio.SetActive(false);
        PantallaInicio.SetActive(true);
    }

    
    void Update()
    {
        if (Input.anyKeyDown)
        {
            MenuInicio.SetActive(true);
            PantallaInicio.SetActive(false);
        }

    }
}
