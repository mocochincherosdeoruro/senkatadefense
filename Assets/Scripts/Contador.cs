using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Code
{
    public class Contador : MonoBehaviour
    {
        public Text PuntosA;
        public int ContadorP;
        public GameObject Counter;
        //private PlayFabUpdatePlayerStatistics _playFabUpdatePlayerStatistics;
        private const string LeaderboardName = "LeaderboardTest";
        // Start is called before the first frame update
        void Start()
        {
            ContadorP = 0;
        }

        // Update is called once per frame
        void Update()
        {

        }
        public void ContadorPuntos(int Racha)
        {
            ContadorP = ContadorP + Racha;
            PuntosA.text = "Puntos: " + ContadorP;
        }
        //public void FinJuego()
        //{
        //    FindObjectOfType<Main>().PointsGet(ContadorP);
        //}
    }
}


