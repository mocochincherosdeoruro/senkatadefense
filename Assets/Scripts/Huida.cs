using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Code
{
    public class Huida : MonoBehaviour
    {
        public NavMeshAgent Llunku;
        GameObject Respawn;
        GameObject Polecia;
        // Start is called before the first frame update
        void Start()
        {
            Llunku = GetComponent<NavMeshAgent>();
            Respawn = FindObjectOfType<RespawnV>().transform.gameObject;
        }

        // Update is called once per frame
        void Update()
        {
            transform.LookAt(Camera.main.transform);
            Llunku.destination = Respawn.transform.position;
            //FindObjectOfType<Contador>().ContadorPuntos(-15);
        }
    }

}