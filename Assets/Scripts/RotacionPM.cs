using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotacionPM : MonoBehaviour
{
    [SerializeField]
    public Transform Arma;
    public Rigidbody Bala;
    public Transform PuntoS;
    public GameObject Mouser;
    bool active;

    public GameObject Sound;

    // Start is called before the first frame update
    void Start()
    {
        Mouser.SetActive(true);
        active = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        Ray Rasho = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit Info;
        if (Physics.Raycast(Rasho, out Info))
        {
            
            if (Info.collider != null)
            {
                Vector3 direccion = Info.point - Arma.position;
                Arma.rotation = Quaternion.LookRotation(direccion);
                if (active == true)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        Rigidbody _Bala;
                        _Bala = Instantiate(Bala, PuntoS.position, Arma.rotation) as Rigidbody;
                        _Bala.AddForce(PuntoS.up * 5000f);
                        Destroy(_Bala, 0.9f);
                        Instantiate(Sound, PuntoS.position, Arma.rotation);
                    }
                }
                
            }
        }
    }
    void Shoot()
    {
    }
}
