using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerM : MonoBehaviour
{
    public float TiempoJuego;
    public GameObject GameOver;
    public GameObject Shooter;
    public Text Contador;
    // Start is called before the first frame update
    void Start()
    {
        GameOver.SetActive(false);
        Shooter.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        Contador.text = TiempoJuego+"";
        TiempoJuego -= Time.deltaTime;
        if(TiempoJuego<=0)
        {
            Shooter.SetActive(false);
            Cursor.visible = true;
            GameOver.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
