using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PantallaCarga : MonoBehaviour
{
    public int Escena;
    public float TiempoEspera;
    public bool CargaDeEscena;
    // Start is called before the first frame update
    void Start()
    {
        CargaDeEscena = false;
        Time.timeScale = 1;
    }
    void Update()
    {
        TiempoEspera -= Time.deltaTime;
        if(CargaDeEscena==false)
        {
            if (TiempoEspera <= 0)
            {
                CargarNivel(Escena);
                CargaDeEscena = true;
                Debug.Log("Escena Cargada");
            }
        }
        
    }
    public void CargarNivel(int NumeroDeEscena)
    {
        StartCoroutine(CargarAsync(NumeroDeEscena));
    }
    IEnumerator CargarAsync(int NumeroDeEscena)
    {
        AsyncOperation Operation = SceneManager.LoadSceneAsync(NumeroDeEscena);
        while(!Operation.isDone)
        {
            float Progreso = Mathf.Clamp01(Operation.progress / .9f);
            Debug.Log(Progreso);
            yield return null;
        }
    }
}
