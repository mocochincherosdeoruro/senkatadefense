using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnV : MonoBehaviour
{
    public GameObject[] Enemigo = new GameObject[3];
    int TipoEn;
    public int CantEn;
    public float timerR;
    // Start is called before the first frame update
    void Start()
    {
        TipoEn = Random.Range(0, 3);
        CantEn = Random.Range(1, 5);
        timerR = Random.Range(5f, 15f);
    }

    // Update is called once per frame
    void Update()
    {
        timerR -= Time.deltaTime;
        if (timerR <= 0)
        {
            while(CantEn >= 0)
            {
                Instantiate(Enemigo[Random.Range(0,5)], transform.position, transform.rotation);
                CantEn--;               
            }
            CantEn = Random.Range(1, 5);
            timerR = Random.Range(5f, 15f);
        }
    }
}
