﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenuUI;
    public GameObject UI;
    public GameObject Apuntado;
    [SerializeField] private bool isPaused;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;
            
        }
        if(isPaused)
        {
            ActMenu();
        }else
        {

            Cursor.visible = false;
            DesMenu();
        }
    }
    void ActMenu()
    {
        Time.timeScale = 0;
        AudioListener.pause = true;
        pauseMenuUI.SetActive(true);
        UI.SetActive(false);
        Apuntado.SetActive(false);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

    }
    public void DesMenu()
    {
        Time.timeScale = 1;
        AudioListener.pause = false;
        pauseMenuUI.SetActive(false);
        isPaused = false;
        UI.SetActive(true);
        Apuntado.SetActive(true);
    }
}
