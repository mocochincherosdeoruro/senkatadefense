using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.Collections;
using TMPro;
using System.Text.RegularExpressions;
using PlayFab.ProfilesModels;

[Serializable]
public class InitialUserData
{
    public int InitialPoints;
}


public class PlayFabLogin : MonoBehaviour
{
    public Text txtMail, txtPass;
    private string userEmail, userPassword, userName, inputUsername;
    private int puntos;
    public static readonly Dictionary<string, int> dictionaryVirtualCurrency = new Dictionary<string, int>();

    public Slider slider1;
    public GameObject textoPorcentaje, objeto, panelRegistro, mensajeErrorNick, mensajeErrorNickEng, logo, txtName, txtNameEng, errorNick, errorNickEng,
        buttonOk, buttonOkEng, buttonRecover, buttonRecoverEng, politica, panelEsp, panelEng;
    string hora, myPlayFabeId, idioma = "en";
    int mundoActual, cargaSlider = 0;


    void Awake()
    {
        if (string.IsNullOrEmpty(PlayFabSettings.TitleId))
        {
            PlayFabSettings.TitleId = "XXXXXXXXX";
        }


        logo.SetActive(true);
        userEmail = PlayerPrefs.GetString("EMAIL");
        userPassword = PlayerPrefs.GetString("PASSWORD");
        userName = PlayerPrefs.GetString("NAME");

        if (userName.Length > 1)
        {
            var requestAndroid = new LoginWithAndroidDeviceIDRequest() { AndroidDeviceId = SystemInfo.deviceUniqueIdentifier, CreateAccount = true };
            PlayFabClientAPI.LoginWithAndroidDeviceID(requestAndroid, OnLoginSuccess, OnLoginFailure);
            Debug.Log("userName.Length > 1");
        }
        else
        {
            Debug.Log("else");
            logo.SetActive(false);
            RegisterAgain();
        }
    }

    public void RegisterAgain()
    {
        Debug.Log("RegisterAgain");
        panelRegistro.SetActive(true);

        var requestAndroid = new LoginWithAndroidDeviceIDRequest() { AndroidDeviceId = SystemInfo.deviceUniqueIdentifier, CreateAccount = true };
        PlayFabClientAPI.LoginWithAndroidDeviceID(requestAndroid, LoginFake, NoLoginFake);
    }


    //******************************************************************************
    // **************************** Login ***************************
    //******************************************************************************
    private void OnLoginSuccess(LoginResult result)
    {
        Debug.Log("OnLoginSuccess");
        SceneManager.LoadScene("PantallaCarga");
    }

    private void OnLoginFailure(PlayFabError error)
    {
        Debug.Log("OnLoginFailure");
        logo.SetActive(false);
        RegisterAgain();
        //throw new NotImplementedException();
        //var registerRequest = new RegisterPlayFabUserRequest { Email = userEmail, Password = userPassword, Username = userName, DisplayName = userName };
        //PlayFabClientAPI.RegisterPlayFabUser(registerRequest, OnRegisterSuccess, OnRegisterFailure);
    }

    public void NoLoginFake(PlayFabError obj)
    {
        Debug.Log("NoLoginFake");
        StartCoroutine("RepeatNoLoginFake");
    }

    IEnumerator RepeatNoLoginFake()
    {
        Debug.Log("NoLoginFake");

        yield return new WaitForSeconds(1f);
        RegisterAgain();
        //throw new NotImplementedException();
    }
    private void LoginFake(LoginResult obj)
    {
        Debug.Log("LoginFake");
        //var requestAndroid = new LoginWithAndroidDeviceIDRequest() { AndroidDeviceId = SystemInfo.deviceUniqueIdentifier, CreateAccount = true };
        //PlayFabClientAPI.LoginWithAndroidDeviceID(requestAndroid, OnLoginSuccess, NoLoginFake);
        //throw new NotImplementedException();
    }

    public void RecoverAccount()
    {
        var requestAndroid = new LoginWithAndroidDeviceIDRequest() { AndroidDeviceId = SystemInfo.deviceUniqueIdentifier, CreateAccount = true };
        PlayFabClientAPI.LoginWithAndroidDeviceID(requestAndroid, OnLoginSuccess, NoLoginFake);

    }


    //******************************************************************************
    // **************************** Registro ****************************
    //******************************************************************************
    public void OnClickLogin()
    {
        errorNick.SetActive(false);
        errorNickEng.SetActive(false);
        if (panelEsp.activeSelf)
        {
            userName = txtName.GetComponent<TextMeshProUGUI>().text;

        }
        else
        {
            userName = txtNameEng.GetComponent<TextMeshProUGUI>().text;

        }


        Debug.Log("clickloginOK");
        buttonOk.SetActive(false);
        buttonOkEng.SetActive(false);
        buttonRecover.SetActive(false);
        buttonRecoverEng.SetActive(false);
        userName = userName.Substring(0, userName.Length - 1);
        PlayFabClientAPI.GetAccountInfo(new GetAccountInfoRequest
        {
            TitleDisplayName = userName
        }, nameRepetido, nameNuevo);

    }
    private void nameRepetido(GetAccountInfoResult obj)
    {
        Debug.Log("nameRepetido");
        buttonOk.SetActive(true);
        buttonOkEng.SetActive(true);
        buttonRecover.SetActive(true);
        buttonRecoverEng.SetActive(true);

        if (errorNick.activeSelf)
        {
            errorNick.SetActive(false);
        }
        errorNick.SetActive(true);

        if (errorNickEng.activeSelf)
        {
            errorNickEng.SetActive(false);
        }
        errorNickEng.SetActive(true);
    }

    private void nameNuevo(PlayFabError obj)
    {
        Debug.Log("nameNuevo");
        if (panelEsp.activeSelf)
        {
            userEmail = txtName.GetComponent<TextMeshProUGUI>().text + "@mail.com";
            userPassword = txtName.GetComponent<TextMeshProUGUI>().text;
        }
        else
        {
            userEmail = txtNameEng.GetComponent<TextMeshProUGUI>().text + "@mail.com";
            userPassword = txtNameEng.GetComponent<TextMeshProUGUI>().text;
        }
        //var registerRequest = new RegisterPlayFabUserRequest { Email = userEmail, Password = userPassword, Username = userName, DisplayName = userName };
        //PlayFabClientAPI.RegisterPlayFabUser(registerRequest, OnRegisterSuccess, OnRegisterFailure);

        var requestAndroid = new LoginWithAndroidDeviceIDRequest { AndroidDeviceId = SystemInfo.deviceUniqueIdentifier, CreateAccount = true };
        PlayFabClientAPI.LoginWithAndroidDeviceID(requestAndroid, OnRegisterSuccess, OnRegisterFailure);
    }

    private void OnRegisterFailure(PlayFabError result)
    {
        Debug.Log("OnRegisterFailure");
        buttonOk.SetActive(true);
        buttonOkEng.SetActive(true);
        buttonRecover.SetActive(true);
        buttonRecoverEng.SetActive(true);
        Debug.Log(result.GenerateErrorReport());

        if (errorNick.activeSelf)
        {
            errorNick.SetActive(false);
        }
        errorNick.SetActive(true);

        if (errorNickEng.activeSelf)
        {
            errorNickEng.SetActive(false);
        }
        errorNickEng.SetActive(true);
    }

    private void OnRegisterSuccess(LoginResult result)
    {
        Debug.Log("OnRegisterSuccess");
        PlayerPrefs.SetString("PlayfabId", result.PlayFabId);
        UpdatePlayerData("PlayfabId", result.PlayFabId);
        PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = userName
        }, RegistroCompletado, ErrorRegistro);
    }

    private void RegistroCompletado(UpdateUserTitleDisplayNameResult result)
    {
        errorNick.SetActive(false);
        errorNickEng.SetActive(false);
        objeto.SetActive(true);
        politica.SetActive(false);
        PlayerPrefs.SetString("NAME", userName);
        PlayerPrefs.SetString("EMAIL", userName + "@mail.com");
        PlayerPrefs.SetString("PASSWORD", userName + userName);

        Debug.Log("Login SUCCESS !!");
        PlayerPrefs.SetInt("anuncioVisto", 0);
        PlayerPrefs.SetInt("anuncioVistoPVP", 0);

        cargaSlider = 0;
        slider1.value = 0;
        StartCoroutine("CargarJuego");
        StartCoroutine("CreateUsers");
    }

    private void ErrorRegistro(PlayFabError result)
    {
        buttonOk.SetActive(true);
        buttonOkEng.SetActive(true);
        buttonRecover.SetActive(true);
        buttonRecoverEng.SetActive(true);
        Debug.Log(result.Error.ToString());

        errorNick.SetActive(true);
        errorNickEng.SetActive(true);
    }

    IEnumerator CargarJuego()
    {
        cargaSlider += UnityEngine.Random.Range(2, 4);
        yield return new WaitForSeconds(0.05f);
        slider1.value = (float)cargaSlider / 100;

        textoPorcentaje.GetComponent<TextMeshProUGUI>().text = (slider1.value * 100).ToString() + " %";

        if (cargaSlider < 100)
        {
            StartCoroutine("CargarJuego");
        }
    }

    IEnumerator CreateUsers()
    {
        createUserData1();
        yield return new WaitForSeconds(0.66f);
        createUserData2();
        yield return new WaitForSeconds(0.66f);
        createUserData3();
        yield return new WaitForSeconds(0.66f);
        createUserData4();
        yield return new WaitForSeconds(0.66f);
        GetTitleId();
        yield return new WaitForSeconds(0.66f);
        SceneManager.LoadScene("PantallaCarga");
    }

    private void createUserData1()
    {
        PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest()
        {
            FunctionName = "createPlayerData1",
            FunctionParameter = new { inputValue = "PARAMETER" },
            GeneratePlayStreamEvent = true,
        }, null, null);
    }
    private void createUserData2()
    {
        PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest()
        {
            FunctionName = "createPlayerData2",
            FunctionParameter = new { inputValue = "PARAMETER" },
            GeneratePlayStreamEvent = true,
        }, null, null);
    }
    private void createUserData3()
    {
        PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest()
        {
            FunctionName = "createPlayerData3",
            FunctionParameter = new { inputValue = "PARAMETER" },
            GeneratePlayStreamEvent = true,
        }, null, null);
    }
    private void createUserData4()
    {
        PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest()
        {
            FunctionName = "createPlayerData4",
            FunctionParameter = new { inputValue = "PARAMETER" },
            GeneratePlayStreamEvent = true,
        }, null, null);
    }


    void GetTitleId()
    {
        if (PlayerPrefs.GetString("Idioma") == "Spanish")
        {
            idioma = "es";
        }

        PlayFabClientAPI.GetPlayerCombinedInfo(new GetPlayerCombinedInfoRequest
        {
            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
            {
                GetUserAccountInfo = true
            }
        },
        result =>
        {
            PlayerPrefs.SetString("PlayerIdTitle", result.InfoResultPayload.AccountInfo.TitleInfo.TitlePlayerAccount.Id);
            UpdatePlayerData("PlayerIdTitle", result.InfoResultPayload.AccountInfo.TitleInfo.TitlePlayerAccount.Id);
            SetProfileLanguage(idioma, 0, result.InfoResultPayload.AccountInfo.TitleInfo.TitlePlayerAccount.Id);
        },
        fail =>
        {
            Debug.Log(fail.GenerateErrorReport());
        });
    }

    void SetProfileLanguage(string language, int? profileExpectedVersion, string stringId)
    {
        var request = new SetProfileLanguageRequest
        {
            Language = language,
            ExpectedVersion = profileExpectedVersion,
            Entity = new PlayFab.ProfilesModels.EntityKey
            {
                Id = stringId,
                Type = "title_player_account"
            },    
        };
        PlayFabProfilesAPI.SetProfileLanguage(request, res =>
        {
            Debug.Log("The language on the entity's profile has been updated.");
        }, FailureCallback);
    }

    void FailureCallback(PlayFabError error)
    {
        Debug.LogWarning("Something went wrong with your API call. Here's some debug information:");
        Debug.LogError(error.GenerateErrorReport());
    }

    public void UpdatePlayerData(string key, string value)
    {
        PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>() {
                {key, value}
            },
            Permission = UserDataPermission.Public
        }, null, null);
     //Mostrar PLAYER DATA
    void GetUserData(string myPlayFabeId)
    {
        PlayFabClientAPI.GetUserData(new GetUserDataRequest()
        {
            PlayFabId = myPlayFabeId,
            Keys = null
        }, result =>
        {
            /* foreach (var pair in result.Data)
             {
                 Debug.Log(pair.Key + " -- " + result.Data[pair.Key].Value);
             }*/
        }, (error) =>
        {
            Debug.Log("Got error retrieving user data:");
        });

    }

}
}